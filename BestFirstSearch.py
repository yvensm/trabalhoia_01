from Node import Node

def bestFirstSearch(problem,heuristic):
    node = Node(problem.initial)
    border = []
    visitedList = []

    node.cost=heuristic[node.state]
    border.append(node)
    while border:
        border = borderSort(border)
        #printBorderWithCost(border)
        if problem.goal_test(node.state):
            return solution(node)
        node = border.pop(-1)
        noAtual = node.state
        if noAtual != "None":
            visitedList.append(noAtual)
        for action in problem.actions(node.state):
            child = Node(action, node)
            child.cost = heuristic[child.state]
            if checkNode(border, visitedList, child):
                border.append(child)
    return None


def checkNode(border,visitedList,child):
    for i in border:
        if(child.state == i.state):
            if(child.cost < i.cost):
                i=child
            return False
    for i in visitedList:
        if(child.state == i):
            return False
    return True

def borderSort(border):
    for i in range(len(border)):
        for j in range(i+1,len(border)):
            if border[i].cost <= border[j].cost:
                aux=border[i]
                border[i]=border[j]
                border[j]=aux

    return border


def solution(node):
    lista = []
    while node:
        lista.append(node.state)
        node = node.parent
    lista.reverse()
    print("Solution:")
    return lista


def printBorderWithCost(border):
    print("Boder:", end=' ')
    for node in border:
        print('[',end='')
        print(node.state + ", "+str(node.cost)+'] ',end='')
    print("\n\n")

