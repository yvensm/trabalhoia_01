from Node import Node

def BFS(problem):
    node = Node(problem.initial)
    border=[]
    visitedList=[]
    noAtual = "None"
    # print(listBorder(border))
    # print("Visited List: ",end='')
    # print(visitedList)
    # print("Current Node : " + noAtual)
    # print("\n")
    border.append(node)
    while border:
        #
        # print("Border",end=": ")
        # print(listBorder(border))
        # print("Visited List: ", end='')
        # print(visitedList)
        # print("Current Node : " + noAtual)
        # print("\n")
        if problem.goal_test(node.state):
            return solution(node)
        node = border.pop(0)
        noAtual=node.state
        if noAtual != "None":
            visitedList.append(noAtual)
        for action in problem.actions(node.state):
            child = Node(action, node)
            if child not in border and child not in visitedList:
                border.append(child)
    return None


def BFSWithVisited(problem):
    node = Node(problem.initial)
    border=[]
    visitedList=[]
    noAtual = "None"
    # print(listBorder(border))
    # print("Visited List: ",end='')
    # print(visitedList)
    # print("Current Node : " + noAtual)
    # print("\n")
    border.append(node)
    while border:

        # print("Border",end=": ")
        # print(listBorder(border))
        # print("Visited List: ", end='')
        # print(visitedList)
        # print("Current Node : " + noAtual)
        # print("\n")
        if problem.goal_test(node.state):
            return solution(node)
        node = border.pop(0)
        noAtual=node.state
        if noAtual != "None":
            visitedList.append(noAtual)
        for action in problem.actions(node.state):
            child = Node(action, node)
            if child not in border and child not in visitedList:
                border.append(child)
    return None
def solution(node):
    lista = []
    while node:
        lista.append(node.state)
        node = node.parent
    lista.reverse()
    print("Solution:",end='')
    return lista

def listBorder(border):
    lista = []
    for node in border:
       lista.append(node.state)
    lista.reverse()
    return lista