from Node import Node

def bidirectionalSearch(problem):
    node_a= Node(problem.initial)
    node_b= Node(problem.goal)

    border_a=[]
    border_b=[]
    visitedList_a = []
    visitedList_b = []

    intersecting=0
    node = Node(problem.initial)
    border = []
    noAtual = "None"

    border_a.append(node_a)
    border_b.append(node_b)

    while border_a:

        intersecting=isIntersecting(visitedList_a,visitedList_b)

        if(intersecting != 0):
            return solutionBidiretional(intersecting[0],intersecting[1])

        node_a = border_a.pop(0)
        node_b = border_b.pop(0)
        visitedList_a.append(node_a)
        visitedList_b.append(node_b)

        # print("BORDA A:",end='')
        # print(border_a)
        # print("BORDAR B:",end='')
        # print(border_b)


        for action in problem.actions(node_a.state):
            child_a = Node(action, node_a)
            if child_a not in border:
                if child_a not in visitedList_a:
                    border_a.append(child_a)
        for action in problem.actions(node_b.state):
            child_b = Node(action, node_b)
            if child_b not in border_b:
                if child_b not in visitedList_b:
                    border_b.append(child_b)
    return None

def solutionBidiretional(node_a,node_b):
    lista = []
    while node_a:
        lista.append(node_a.state)
        node_a=node_a.parent
    lista.reverse()
    while node_b:
        if(node_b.state == lista[len(lista)-1]):
            if node_b.parent:
                node_b = node_b.parent
            else:
                break
        lista.append(node_b.state)
        node_b=node_b.parent
    return lista

def isIntersecting(visited_a,visited_b):
    list=[]
    for a in visited_a:
        for b in visited_b:
            if(a.state == b.state):
                list.append(a)
                list.append(b)
                return list
    return 0
