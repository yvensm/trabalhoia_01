package searches;

import java.util.*;
import model.Node;
import graphs.Graph;
public class BFS {
	private Queue<Node> myQueue;
	private List<Node> visitedList;
	
	public BFS() {
		this.myQueue = new LinkedList<>();
		this.visitedList= new ArrayList<>();
	}
	
	public void bfs(Graph g) {
		List<Node> nodeList = g.getAllNodes();
		for(Node n : nodeList) {
			if(n.isVisited() == false) {
				n.setVisited(true);
				bsfInQueue(n);
			}
		}
	}
	
	private void bsfInQueue(Node n) {
		this.myQueue.add(n);
		
		while(this.myQueue.isEmpty() == false) {
			printBorder();
			Node currentNode = this.myQueue.remove();
			
			printVisitedList();
			System.out.println("Current Node: "+ currentNode.getElement().toString());
			this.visitedList.add(currentNode);
			for(Node node : currentNode.getNeighbours()) {
				if(node.isVisited() == false) {
					node.setVisited(true);
					this.myQueue.add(node);
				}
			}
		}
		printBorder();
		printVisitedList();
	}
	private void printVisitedList() {
		System.out.print("V=[");
		for(Node no:this.visitedList) {
			
			System.out.print(" "+no.getElement().toString());
		}
		System.out.print(" ] ");
		
	}
	private void printBorder() {
		System.out.print("B=[");
		for(Node no:this.myQueue) {
			
			System.out.print(" "+no.getElement().toString());
		}
		System.out.print(" ]");
		
	}
}
