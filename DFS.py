from Node import Node


def DFS(problem):
    node = Node(problem.initial)
    border = []
    noAtual = "None"
    # print("Border", end=": ")
    # print(listBorder(border))
    # print("Current Node : " + noAtual)

    if problem.goal_test(node.state):
        return [node.state]

    border.append(node)
    while border:
        # print("Border", end=": ")
        # print(listBorder(border))
        # print("Current Node : " + noAtual)
        if problem.goal_test(node.state):
            return solution(node)
        node = border.pop(-1)

        noAtual=node.state
        for action in problem.actions(node.state):
            child = Node(action, node)
            if child not in border:
                border.append(child)
    return None

def DFSWithVisited(problem):
    node = Node(problem.initial)
    border=[]
    visitedList=[]

    border.append(node)
    while border:

        if problem.goal_test(node.state):
            return solution(node)
        node = border.pop(-1)
        visitedList.append(node.state)
        for action in problem.actions(node.state):
            child = Node(action, node)
            if child not in border and child.state not in visitedList:
                border.append(child)
    return None




def solution(node):
    lista = []
    while node:
        lista.append(node.state)
        node = node.parent
    lista.reverse()
    print("Solution:",end='')
    return lista

def listBorder(border):
    lista = []
    for node in border:
       lista.append(node.state)
    lista.reverse()
    return lista


