from Node import Node

def DFS(problem):
    node = Node(problem.initial)
    if problem.goal_test(node.state):
        return [node.state]
    frontier = [node]

    while frontier:
        
        node = frontier.pop(-1)
        print(problem.actions(node.state))
        for action in problem.actions(node.state):
            child = Node(action, node)
            child.state = problem.result(node.state,action)
            if child not in frontier:
                if problem.goal_test(child.state):
                    return solution(child)
                frontier.append(child)
    return None

def solution(node):
    lista = []
    while node:
        lista.append(node.state)
        node = node.parent
    lista.reverse()
    return lista