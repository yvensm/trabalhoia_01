import random

class Problem:

    def __init__(self, initial, goal, graph=None):
         self.initial = initial
         self.goal = goal
         self.graph = graph

    def actions(self, nodeState):
        lista = list(self.graph[nodeState])
        lista = random.shuffle(lista)
        return list(self.graph[nodeState])

    def result(self, state, action):
        return action

    def path_cost(self, cost_so_far, A, action, B):
        return cost_so_far + (self.graph.get(A, B) or infinity)

    def find_min_edge(self):
        m = infinity
        for d in self.graph.graph_dict.values():
            local_min = min(d.values())
            m = min(m, local_min)

        return m

    def goal_test(self, state):

        if isinstance(self.goal, list):
            return is_in(state, self.goal)
        else:
            return state == self.goal

    def actionsWithCost(self, nodeState):
        list = []
        lin = 0
        for item in self.actions(nodeState):
            list.append([])
            dict = self.graph[nodeState]
            list[lin].append(item)
            list[lin].append(dict[item])
            lin += 1
        return list

    def getCost(self, init, final):
        dict = self.graph[init]
        return dict[final]