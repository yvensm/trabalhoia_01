map_romania = dict(
    Arad=dict(Zerind=75, Sibiu=140, Timisoara=118),
    Zerind=dict(Arad=75, Oradea=71),
    Timisoara=dict(Arad=118, Lugoj=111),
    Mehadia=dict(Lugoj=70, Drobeta=75),
    Bucharest=dict(Urziceni=85, Pitesti=101, Giurgiu=90, Fagaras=211),
    Craiova=dict(Drobeta=120, Rimnicu=146, Pitesti=138),
    Drobeta=dict(Mehadia=75, Craiova=120),
    Eforie=dict(Hirsova=86),
    Fagaras=dict(Sibiu=99, Bucharest=211),
    Hirsova=dict(Urziceni=98, Eforie=86),
    Vaslui=dict(Iasi=92, Urziceni=142),
    Iasi=dict(Vaslui=92, Neamt=87),
    Neamt=dict(Iasi=87),
    Giurgiu=dict(Bucharest=90),
    Lugoj=dict(Timisoara=111, Mehadia=70),
    Oradea=dict(Zerind=71, Sibiu=151),
    Pitesti=dict(Rimnicu=97, Craiova=138, Bucharest=101),
    Sibiu=dict(Oradea=151, Arad=140, Rimnicu=80, Fagaras=90),
    Rimnicu=dict(Sibiu=80, Pitesti=97, Craiova=146),
    Urziceni=dict(Vaslui=142, Bucharest=85, Hirsova=98))

HSLD = dict(
    Arad=366,
    Bucharest=0,
    Craiova=160,
    Drobeta=242,
    Eforie=161,
    Fagaras=176,
    Giurgiu=77,
    Hirsova=151,
    Iasi=226,
    Lugoj=244,
    Mehadia=241,
    Neamt=234,
    Oradea=380,
    Pitesti=100,
    Rimnicu=193,
    Sibiu=253,
    Timisoara=329,
    Urziceni=80,
    Vaslui=199,
    Zerind=374
)
""" [Figure 4.9]
Eight possible states of the vacumm world
Each state is represented as
   *       "State of the left room"      "State of the right room"   "Room in which the agent
                                                                      is present"
1 - DDL     Dirty                         Dirty                       Left
2 - DDR     Dirty                         Dirty                       Right
3 - DCL     Dirty                         Clean                       Left
4 - DCR     Dirty                         Clean                       Right
5 - CDL     Clean                         Dirty                       Left
6 - CDR     Clean                         Dirty                       Right
7 - CCL     Clean                         Clean                       Left
8 - CCR     Clean                         Clean                       Right
"""
vacuum_world = dict(
    State_1=dict(Suck=['State_7', 'State_5'], Right=['State_2']),
    State_2=dict(Suck=['State_8', 'State_4'], Left=['State_2']),
    State_3=dict(Suck=['State_7'], Right=['State_4']),
    State_4=dict(Suck=['State_4', 'State_2'], Left=['State_3']),
    State_5=dict(Suck=['State_5', 'State_1'], Right=['State_6']),
    State_6=dict(Suck=['State_8'], Left=['State_5']),
    State_7=dict(Suck=['State_7', 'State_3'], Right=['State_8']),
    State_8=dict(Suck=['State_8', 'State_6'], Left=['State_7'])
    )
vacuum_word2= dict(
    SSE = dict(LSE = 1, SSD = 1, SSE=1),
    SSD = dict(SLD = 1, SSE= 1,SSD = 1),
    SLE = dict(LLE= 1, SLD =1,SLE=1),
    SLD = dict(SLD= 1, SLE= 1),
    LSE = dict(LSE=1 , LSD = 1),
    LSD = dict(LLD =1, LSD=1, LSE = 1),
    LLE = dict(LLE = 1, LLD=1),
    LLD = dict(LLD = 1, LLE=1)
)