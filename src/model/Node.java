package model;

import java.util.*;

public class Node {
	private Object element;
	private boolean visited;
	private List<Node> neighbours;
	
	public Node (Object e) {
		this.element = e;
		this.visited = false;
		this.neighbours = new ArrayList<>();
	}

	public boolean isVisited() {
		return this.visited;
	}
	
	public void setVisited(boolean v) {
		this.visited = v;
	}
	
	public void addNeighbour(Node n) {
		this.neighbours.add(n);
	}
	
	public List<Node> getNeighbours(){
		return this.neighbours;
	}
	
	public void setNeighbours(List<Node> neighbours) {
		this.neighbours = neighbours;
	}
	
	public Object getElement() {
		return element;
	}
	
	public void setElement(Object element) {
		this.element = element;
	}
	
}
