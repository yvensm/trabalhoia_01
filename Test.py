import timeit

from BFS import BFS
from DFS import *
from BidirectionalSearch import bidirectionalSearch
from DLS import depthLimitedSearch
from IDS import iterativeDeepeningSearch
from UCS import uniformCostSearch
from BestFirstSearch import bestFirstSearch


from Problem import Problem
from Worlds import *

INIT_MAP = "Oradea"
GOAL_MAP = "Bucharest"
INIT_PUZZLE=['1','0','3','4','2','5','7','8','6']

INIT_ASP='SSE'
GOAL_FIM='LLD'


print("Busca em largura com lista de visitados")
for i in range(1,11):

    inicio = timeit.default_timer()

    print(DFS(Problem(INIT_ASP,GOAL_FIM,vacuum_word2)))

    fim = timeit.default_timer()

    print ('%i duracao: %f' % (i,(fim - inicio)))